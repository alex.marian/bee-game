const state = {
    username: '',
};

const getters = {
    getUsername: state => state.username,
};

const mutations = {
    USER_NAME(state, value) {
        state.username = value;
    },
};

const actions = {
    setUsername({ commit, }, value) {
        value = sessionStorage.getItem('username');
        commit('USER_NAME', value);
    },
};
export default {
    state,
    getters,
    mutations,
    actions,
};
