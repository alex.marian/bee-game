import {shallowMount, createLocalVue} from '@vue/test-utils'
import Bees from '@/components/Bees.vue'

let localVue = createLocalVue();

describe('Bees.vue', () => {
    let wrapper;
    let bees = [
      {
        id: 1,
        type: '',
        hp: 100,
        damage: 8,
        hit: false,
        died: false
      }
    ];
    beforeEach(() => {
        wrapper = shallowMount(Bees, {
            localVue,
            propsData: {
                bees: bees,
            },
        });
    });
    it('renders without errors', () => {
        expect(wrapper.isVueInstance()).toBeTruthy();
    });
    it('expect bees props to be an array with data', () => {
        expect(wrapper.vm.bees).toBe(bees);
    });
})
