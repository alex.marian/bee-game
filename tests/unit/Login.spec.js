import {shallowMount, createLocalVue} from '@vue/test-utils'
import Login from '@/components/Login.vue'
import TestHelpers from './TestHelper';

let localVue = createLocalVue();

describe('Login.vue', () => {
    let wrapper,
        h;
    beforeEach(() => {
        wrapper = shallowMount(Login, {
            localVue,
        });
        h = new TestHelpers(wrapper, expect);
    });
    it('renders without errors', () => {
        expect(wrapper.isVueInstance()).toBeTruthy();
    });
    it('received error if username data is empty', () => {
        wrapper.vm.$data.username = '';
        h.click('#play-game');
        expect(wrapper.vm.hasError).toBe(true);
    });
    it('pass validation if username data is not empty', () => {
        wrapper.vm.$data.username = 'Alex';
        h.click('#play-game');
        expect(wrapper.vm.hasError).toBe(false);
    })
})
