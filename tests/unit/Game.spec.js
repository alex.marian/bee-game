import {shallowMount, createLocalVue} from '@vue/test-utils'
import Game from '@/components/Game.vue'
import TestHelpers from './TestHelper';
import Vuex from 'vuex'

let localVue = createLocalVue();

localVue.use(Vuex);

describe('Game.vue', () => {
    let wrapper,
        getters,
        store,
        h;
    beforeEach(() => {
        getters = {
            getUsername: () => {
                return 'Alex'
            },
        };

        store = new Vuex.Store({
            getters,
        });

        wrapper = shallowMount(Game, {
            localVue,
            store
        });
        h = new TestHelpers(wrapper, expect);
    });

    it('renders without errors', () => {
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('populate username paragraph with correct text', () => {
        const text = h.find('#username');
        const username = store.getters.getUsername;

        expect(text.text()).toContain('Welcome ' + username + '.')
    });

    it('queen bee hp and damage check', () => {
        expect(wrapper.vm.bees[0].type).toBe('queen');
        expect(wrapper.vm.bees[0].hp).toBe(100);
        expect(wrapper.vm.bees[0].damage).toBe(8);
    });

    it('worker bee hp and damage check', () => {
        expect(wrapper.vm.bees[1].type).toBe('worker');
        expect(wrapper.vm.bees[1].hp).toBe(75);
        expect(wrapper.vm.bees[1].damage).toBe(10);
    });

    it('drone bee hp and damage check', () => {
        expect(wrapper.vm.bees[10].type).toBe('drone');
        expect(wrapper.vm.bees[10].hp).toBe(50);
        expect(wrapper.vm.bees[10].damage).toBe(12);
    });

    it('game over when queen bee is death (with hp = 0)', () => {
        expect(wrapper.vm.bees[0].type).toBe('queen');
        wrapper.vm.bees[0].hp = 0;
        expect(wrapper.vm.gameOver).toBe(true)
    })
});
